# Article 1er

I. – Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est autorisé à prendre par ordonnances les mesures relevant du domaine de la loi pour tirer les conséquences d’un retrait du Royaume‑Uni de l’Union européenne sans accord conclu conformément à l’article 50 du traité sur l’Union européenne, en matière :

1° De droit d’entrée et de séjour des ressortissants britanniques en France ;

2° D’emploi des ressortissants britanniques exerçant légalement à la date du retrait du Royaume‑Uni de l’Union européenne une activité professionnelle salariée en France ou appelés à y exercer une activité professionnelle salariée au sein d’entreprises installées sur le territoire britannique à la date du retrait du Royaume‑Uni de l’Union européenne ayant fait le choix de se déployer en France après celui‑ci ;

3° D’exercice, par une personne physique ou morale exerçant légalement à la date du retrait du Royaume‑Uni de l’Union européenne, d’une activité ou d’une profession dont l’accès ou l’exercice sont subordonnés au respect de conditions. Les qualifications professionnelles obtenues au Royaume‑Uni sont immédiatement reconnues dès lors que les titulaires de celles‑ci exercent leur activité en France au 30 mars 2019 ou sont appelés à y exercer une activité professionnelle salariée au sein d’entreprises installées sur le territoire britannique à la date du retrait du Royaume‑Uni de l’Union européenne ayant fait le choix de se déployer en France après celui‑ci ;

4° De règles applicables à la situation des agents titulaires et stagiaires de la fonction publique de nationalité britannique ;

5° D’application aux ressortissants britanniques résidant légalement en France à la date du retrait du Royaume‑Uni de l’Union européenne de la législation relative aux droits sociaux et aux prestations sociales ou, au‑delà de cette date, appelés à y exercer une activité professionnelle salariée au sein d’entreprises installées sur le territoire britannique à la date du retrait du Royaume‑Uni de l’Union européenne ayant fait le choix de se déployer en France après celui‑ci ;

6° De contrôle sur les marchandises et passagers à destination et en provenance du Royaume‑Uni et de contrôle vétérinaire et phytosanitaire à l’importation en provenance du Royaume‑Uni ;

7° De réalisation d’opérations de transport routier de marchandises ou de personnes sur le territoire français, y compris en transit, par des personnes physiques ou morales établies au Royaume‑Uni.

Dans les conditions prévues au premier alinéa du présent I, le Gouvernement est également habilité à prendre toute autre mesure nécessaire au traitement de la situation des ressortissants britanniques résidant en France ou y exerçant une activité ainsi que des personnes morales établies au Royaume‑Uni et exerçant une activité en France afin de préserver les intérêts de la France en matière économique, financière, de défense et de sécurité.

II (nouveau). – Les ordonnances prévues au I visent, dans l’attente, le cas échéant, de traités ou d’accords bilatéraux entre la France et le Royaume‑Uni, à tirer les conséquences de l’absence d’accord de retrait du Royaume‑Uni de l’Union européenne, afin de :

1° Régler la situation en France des ressortissants britanniques résidant légalement sur le territoire national au moment du retrait du Royaume‑Uni ;

2° Préserver les activités économiques sur le territoire français ;

3° Préserver les flux de marchandises et de personnes à destination et en provenance du Royaume‑Uni ;

4° Garantir un niveau élevé de sécurité sanitaire en France ;

5° Prévoir des dérogations, des procédures administratives simplifiées et des délais de régularisation pour les personnes morales ou physiques concernées.

Ces ordonnances peuvent prévoir que les mesures accordant aux ressortissants britanniques ou aux personnes morales établies au Royaume‑Uni un traitement plus favorable que celui des ressortissants de pays tiers ou de personnes morales établies dans des pays tiers cesseront de produire effet si le Royaume‑Uni n’accorde pas un traitement équivalent.

III (nouveau). – Les ordonnances prévues au présent article sont prises dans un délai de douze mois à compter de la publication de la présente loi.

# Article 2

I. – Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est autorisé à prendre par ordonnances les mesures relevant du domaine de la loi pour tirer les conséquences d’un retrait du Royaume‑Uni de l’Union européenne sans accord conclu conformément à l’article 50 du traité sur l’Union européenne, en ce qui concerne :

1° La prise en compte, pour l’ouverture et la détermination des droits sociaux, des périodes d’assurance, d’activités ou de formation professionnelle exercées ou effectuées au Royaume‑Uni avant la date de son retrait de l’Union européenne ;

2° La prise en compte des diplômes et des qualifications professionnelles acquis ou en cours d’acquisition et l’expérience professionnelle acquise au Royaume‑Uni à la date de son retrait de l’Union européenne ainsi que les diplômes et qualifications professionnelles s’inscrivant dans le cadre d’un parcours de formation intégrant ceux obtenus ou en cours d’acquisition à cette même date ;

3° La poursuite par les bénéficiaires de licences et d’autorisations de transfert de produits et matériels à destination du Royaume‑Uni, délivrées en application des articles L. 2335‑10 et L. 2335‑18 du code de la défense avant la date du retrait du Royaume‑Uni de l’Union européenne, de la fourniture de ces produits et matériels jusqu’à l’expiration du terme fixé par ces licences et autorisations ;

4° L’accès des entités françaises aux systèmes de règlement interbancaire et de règlement livraison des pays tiers dont le Royaume‑Uni en assurant le caractère définitif des règlements effectués au moyen de ces systèmes, la continuité de l’utilisation des conventions cadres en matière de services financiers et la sécurisation des conditions d’exécution des contrats conclus antérieurement à la perte de la reconnaissance des agréments des entités britanniques en France ;

5° La continuité des flux de transport de passagers et de marchandises entre la France et le Royaume‑Uni à travers le tunnel sous la Manche en vue d’assurer le respect par la France de ses engagements en tant que concédant du tunnel sous la Manche.

II (nouveau). – Les ordonnances prévues au I visent, dans l’attente, le cas échéant, de traités ou d’accords bilatéraux entre la France et le Royaume‑Uni, à :

1° Préserver les droits sociaux et professionnels des ressortissants français et des autres personnes auxquelles le droit de l’Union européenne interdit de réserver un traitement différent ;

2° Préserver les intérêts de la France en matière économique, financière, de défense et de sécurité.

III (nouveau). – Les ordonnances prévues au présent article sont prises dans un délai de douze mois à compter de la publication de la présente loi.

# Article 3

I. – Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est autorisé à prendre par ordonnances les mesures relevant du domaine de la loi rendues temporairement nécessaires par la perspective d’un retrait du Royaume‑Uni de l’Union européenne, afin de prévoir le régime procédural simplifié applicable aux travaux en vue de la construction ou de l’aménagement en urgence de locaux, installations ou infrastructures portuaires, ferroviaires, aéroportuaires et routiers requis par le rétablissement des contrôles des marchandises et des passagers à destination ou en provenance du Royaume‑Uni.

Les ordonnances prévues au présent article peuvent prévoir de rendre applicables aux opérations mentionnées au premier alinéa du présent I directement liées à l’organisation de ces contrôles, des adaptations ou des dérogations, en matière d’aménagement, d’urbanisme, d’expropriation pour cause d’utilité publique, de préservation du patrimoine, de voirie et de transports, de domanialité publique, de commande publique, de règles applicables aux ports maritimes, de participation du public et d’évaluation environnementale, afin de les adapter à l’urgence de ces opérations.

Les adaptations ou dérogations ainsi instituées de façon temporaire, dans le respect des droits et libertés garantis par la Constitution, sont strictement proportionnées à l’objectif de maintien de la sécurité et de la fluidité des flux de passagers ou de marchandises.

II (nouveau). – Les ordonnances prévues au présent article sont prises dans un délai de six mois à compter de la publication de la présente loi.

# Article 4

Pour chacune des ordonnances prévues aux articles 1er à 3, un projet de loi de ratification est déposé devant le Parlement dans un délai de trois mois à compter de sa publication.